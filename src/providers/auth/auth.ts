import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

let ApiUrl = 'http://127.0.0.1:8000/api';

@Injectable()
export class AuthProvider {

  status:any = [];
  constructor(public http: HttpClient) {
    
  }

  authenticate(email:string,password:string){
    return this.http.post(ApiUrl+'/login',{email :email, password :password});
   
  }

}
