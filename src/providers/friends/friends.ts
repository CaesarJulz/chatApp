import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

//static connection temporary
let ApiUrl = 'http://127.0.0.1:8000/api';

@Injectable()
export class FriendsProvider {

  fullname:string = '';
  constructor(public http: HttpClient) {  
  }

  FollowerList(){    
    return this.http.get(ApiUrl + '/ActiveUsers/0');
   }

  PersonalMessage(userid: string){
    return this.http.get(ApiUrl + '/ActiveUsers/' + userid );
  }

  getChatDetails(chatid: string){
    return this.http.get(ApiUrl + '/MessageDecription/' + chatid +'/0/0');
  }

  sendMessage(message: string,reciever: string, sender: string,userid: string){
    return this.http.post(ApiUrl + '/sendMessage',{msg: message, receiver: reciever, sender: sender, id: userid});
  }

  
}
