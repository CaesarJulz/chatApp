import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ChatPage } from '../pages/chat/chat';

import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { FriendsProvider } from '../providers/friends/friends';
import { AuthProvider } from '../providers/auth/auth';
import { HttpClientModule } from '@angular/common/http';


var config = {
  apiKey: "AIzaSyBSEWgZvkTSwsUVylarD7FScxVzjGdhTHc",
  authDomain: "ionic-chat-app-5f50a.firebaseapp.com",
  databaseURL: "https://ionic-chat-app-5f50a.firebaseio.com",
  projectId: "ionic-chat-app-5f50a",
  storageBucket: "ionic-chat-app-5f50a.appspot.com",
  messagingSenderId: "224975151699"
  };

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ChatPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ChatPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    FriendsProvider,
    AuthProvider
  ]
})
export class AppModule {}
