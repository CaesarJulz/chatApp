import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';

import { FriendsProvider } from '../../providers/friends/friends';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  freind: string = '';
  chatid: string = '';
  MesDetails:any = [];
  message: string = '';
  messageIDs: string = '';
  user_id: string = '';
  rcvr: string;
  snder: string;
  

  constructor(
    public db: AngularFireDatabase,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public Friends: FriendsProvider) {

    this.message = this.navParams.get('message');
    this.chatid = this.navParams.get('chatid');
    this.user_id = this.navParams.get('user_id');
    this.rcvr = this.navParams.get('receiver');
    this.snder = this.navParams.get('sender');

    this.Display();
  }

  Display(){
    this.MesDetails = [];
    let itemParse: string;
    this.Friends.getChatDetails(this.chatid).subscribe((details)=>{
      details['chats'].forEach(elem => {
        itemParse = JSON.parse(elem);
        this.MesDetails.push({
          message: itemParse['msg'],
          reciever: itemParse['receiver'],
          sender: itemParse['sender'],
          chatid: itemParse['receiver']+'_'+itemParse['sender']
        });
        
      });

    });
  }

  SendMessage(){
    // console.log(this.message);
    this.Friends.sendMessage(this.message,this.rcvr,this.snder,this.user_id).subscribe((grey)=>{
    this.Display();
    });

    
    this.message = '';

  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatPage');
  }

}
