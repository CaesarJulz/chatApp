import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;

 username:string = '';
 user_id:string = '';

  constructor(public navParams: NavParams) {
   this.username = this.navParams.get('username');
   this.user_id = this.navParams.get('user_id');

  }

 
}
