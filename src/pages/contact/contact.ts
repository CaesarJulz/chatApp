import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FriendsProvider } from '../../providers/friends/friends';
import { ChatPage } from '../chat/chat';
@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  UserAvailableOffline:any = [];
  UserAvailableOnline:any = [];
  ChatAvailable:any = [];
  username: string = '';
  constructor(public Friends: FriendsProvider,
              public navCtrl: NavController,
              public navParams: NavParams) {
        this.username = this.navParams.get('username');

        this.Friends.FollowerList().subscribe((elem) =>{

              elem['offline'].forEach(offval => {
                this.UserAvailableOffline.push({
                    name: offval['name']
                });
              });

              elem['online'].forEach(offval => {
                this.UserAvailableOnline.push({
                    name: offval['name']
                });
              });

              elem['group'].forEach(offval => {
                this.ChatAvailable.push({
                  name: offval['group_name']
                });
              });

      
        });
        
  }

  chatBuddy(buddy: string){
    this.navCtrl.push(ChatPage,{
      friend : buddy,
      username : this.username
    });
 }

}
