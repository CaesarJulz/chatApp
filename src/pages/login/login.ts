import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';

import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  email: string = '';
  password: string = '';
  
  constructor(private auth: AuthProvider,public navCtrl: NavController, public navParams: NavParams) {
    this.email = this.navParams.get('email');
    this.password = this.navParams.get('password');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

  SigIn(){
   let stats: any = [];
    this.auth.authenticate(this.email,this.password).subscribe((profile)=>{
      if(profile['status']=='success'){
       this.navCtrl.push(TabsPage,{
       username: profile['user']['username'],
       user_id: profile['user']['id']
       });
      }else{
        console.log('failed');
      }
    });   
  }

  
}
