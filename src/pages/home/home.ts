import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {AngularFireDatabase } from 'angularfire2/database';
import * as firebase from 'firebase';
import { FriendsProvider } from '../../providers/friends/friends';
import { ChatPage } from '../chat/chat';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  username: string = '';
  user_id: string = '';
  message: string = '';
  heads: any = [];
  chatHeadsContent:any = [];
  nameRecive: any;

  constructor(
    public db: AngularFireDatabase,
    public navCtrl: NavController, 
    public navParams: NavParams,
    public Friends: FriendsProvider) {

    this.username = this.navParams.get('username'); 
    this.user_id = this.navParams.get('user_id');


    let followers_id: any = [];
    let parseItem: string ='';
    let chatHeadear: any = [];
    
    let chatids: string = '';
   

    this.Friends.PersonalMessage(this.user_id).subscribe((_personalMessage)=>{
      
        _personalMessage['chatHeads'].forEach(element => {
          
               element[0]['mess'].forEach(message => {
                parseItem = JSON.parse(message);
               

                chatids = parseItem['receiver']+'_'+parseItem['sender'];
                  if(parseItem['receiver'] > parseItem['sender']){
                  chatids = parseItem['sender']+'_'+parseItem['receiver'];
                }

                   if(chatHeadear.indexOf(chatids) < 0){
                     chatHeadear.push(chatids);  
                 
                     this.chatHeadsContent.push({
                      name: element[0]['name'][0]['name'],
                      chatid: chatids,
                      sender: parseItem['sender'],
                      receiver: parseItem['receiver']
                   }); 
          
                  }
              });
                                  
           });  
        
        
         
        });     


  }

  chatBuddy(chatid: string, sender: string, receiver: string){
    this.navCtrl.push(ChatPage,{
      chatid: chatid,
      sender: sender,
      receiver: receiver,
      user_id: this.user_id
      });
 }

}
